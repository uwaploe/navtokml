// Navtokml creates KML output from MuST navigation data files
package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"image/color"
	"io"
	"io/ioutil"
	"log"
	"math"
	"net/url"
	"os"
	"runtime"
	"strconv"

	must "bitbucket.org/uwaploe/go-must"
	"bitbucket.org/uwaploe/navsvc/pkg/atlas"
	"github.com/BurntSushi/toml"
	ui "github.com/gosuri/uiprogress"
	kml "github.com/twpayne/go-kml"
	"github.com/twpayne/go-kml/icon"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `Usage: navtokml [options] file [file ...]

Generate KML output from one or more MuST navigation files.
`
var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	docName  = flag.String("name", "MuST Tracks", "KML document name")
	outFile  = flag.String("output", "", "Output file name")
	regFile  = flag.String("regions", "", "File of map region definitions")
	useLines = flag.Bool("lines", false,
		"Use line segments to draw regions")
	incDepth = flag.Bool("depth", false, "Include water depth data")
)

type RGBA struct {
	color.RGBA
}

func (c *RGBA) UnmarshalText(text []byte) error {
	var err error
	if string(text[0]) != "#" {
		return fmt.Errorf("Invalid color specification: %q", text)
	}
	x, err := strconv.ParseUint(string(text[1:]), 16, 32)
	if err != nil {
		return err
	}
	c.A = uint8(x >> 24)
	c.R = uint8((x >> 16) & 0xff)
	c.G = uint8((x >> 8) & 0xff)
	c.B = uint8(x & 0xff)
	return nil
}

type mapRegion struct {
	Name      string           `toml:"name"`
	Color     RGBA             `toml:"color"`
	Verticies []kml.Coordinate `toml:"vertex"`
}

type regions struct {
	Region []mapRegion
}

func avgValue(vals []float32) float32 {
	var sum float32
	for _, v := range vals {
		sum += v
	}
	return sum / float32(len(vals))
}

func addRegions(ce *kml.CompoundElement, b []byte) error {
	var data regions
	_, err := toml.Decode(string(b), &data)
	if err != nil {
		return err
	}
	for _, r := range data.Region {
		id := url.PathEscape(r.Name) + "_style"
		ce.Add(
			kml.SharedStyle(
				id,
				kml.PolyStyle(
					kml.Color(r.Color.RGBA),
					kml.Fill(true),
				),
			),
		)
		ce.Add(
			kml.Placemark(
				kml.Name(r.Name),
				kml.StyleURL("#"+id),
				kml.Polygon(
					kml.Extrude(false),
					kml.AltitudeMode("clampToGround"),
					kml.OuterBoundaryIs(
						kml.LinearRing(
							kml.Coordinates(r.Verticies...),
						),
					),
				),
			),
		)
	}
	return nil
}

func addLines(ce *kml.CompoundElement, b []byte) error {
	var data regions
	_, err := toml.Decode(string(b), &data)
	if err != nil {
		return err
	}
	for _, r := range data.Region {
		id := url.PathEscape(r.Name) + "_style"
		ce.Add(
			kml.SharedStyle(
				id,
				kml.LineStyle(
					kml.Color(r.Color.RGBA),
					kml.Width(2.0),
				),
			),
		)
		ce.Add(
			kml.Placemark(
				kml.Name(r.Name),
				kml.StyleURL("#"+id),
				kml.LineString(
					kml.Extrude(false),
					kml.AltitudeMode("clampToGround"),
					kml.Coordinates(r.Verticies...),
				),
			),
		)
	}
	return nil
}

func newSimpleArray(name string) *kml.CompoundElement {
	return &kml.CompoundElement{
		StartElement: xml.StartElement{
			Name: xml.Name{Local: "gx:SimpleArrayData"},
			Attr: []xml.Attr{
				{Name: xml.Name{Local: "name"}, Value: name},
			},
		},
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	var (
		fout io.WriteCloser
		err  error
	)

	if *outFile != "" {
		fout, err = os.Create(*outFile)
		if err != nil {
			log.Fatalf("Cannot create output file: %v", err)
		}
	} else {
		fout = os.Stdout
	}

	regions := kml.Folder(kml.Name("Regions"))
	if *regFile != "" {
		contents, err := ioutil.ReadFile(*regFile)
		if err != nil {
			log.Printf("Cannot read region file: %v", err)
		} else {
			if *useLines {
				err = addLines(regions, contents)
			} else {
				err = addRegions(regions, contents)
			}
			if err != nil {
				log.Printf("Cannot parse region file: %v", err)
			}
		}
	}

	bar := ui.AddBar(len(args) + 1).AppendCompleted().PrependElapsed()
	bar.PrependFunc(func(b *ui.Bar) string {
		if i := b.Current(); i < len(args) {
			return args[i]
		} else {
			return "generating KML"
		}
	})

	schema := kml.Schema("data_schema", "",
		kml.GxSimpleArrayField("depth", "float").Add(kml.DisplayName("Water Depth")),
	)

	shipTrack := kml.GxMultiTrack()
	vehicleTrack := kml.GxMultiTrack()
	ui.Start()
	defer ui.Stop()

	for _, arg := range args {
		fin, err := os.Open(arg)
		if err != nil {
			log.Fatalf("Open %q failed: %v", arg, err)
		}
		defer fin.Close()

		ship := kml.GxTrack(
			kml.AltitudeMode("absolute"),
		)
		vehicle := kml.GxTrack(
			kml.AltitudeMode("absolute"),
			kml.Extrude(true),
		)
		depth := newSimpleArray("depth")

		rdr := must.NewNavReader(fin)
		samples := make(map[int32]bool)
		var lastAlt float32

		for rdr.Scan() {
			rec, ok := rdr.Record().(atlas.MustNavMessage)
			if !ok {
				log.Fatalf("Bad data record type: %T", rec)
			}

			if (rec.Valid & atlas.ValidAltitude) != 0 {
				lastAlt = rec.Altitude
			}

			if !samples[rec.T.Tsec] {
				// One vehicle position per second
				samples[rec.T.Tsec] = true
				c := kml.Coordinate{
					Lon: rec.Lon * 180. / math.Pi,
					Lat: rec.Lat * 180. / math.Pi,
					Alt: float64(-rec.Depth),
				}
				a := kml.GxAngle{
					Heading: float64(rec.Heading) * 180. / math.Pi,
					Tilt:    float64(rec.Pitch) * 180. / math.Pi,
					Roll:    float64(rec.Roll) * 180. / math.Pi,
				}
				vehicle.Add(
					kml.When(rec.T.AsTime()),
					kml.GxCoord(c),
					kml.GxAngles(a),
				)

				if *incDepth {
					depth.Add(kml.GxValue(fmt.Sprintf("%.2f", -rec.Depth-lastAlt)))
				}
			}

			if (rec.Valid & atlas.ValidVesselPos) != 0 {
				ship.Add(
					kml.When(rec.T.AsTime()),
					kml.GxCoord(kml.Coordinate{
						Lon: rec.VLon * 180. / math.Pi,
						Lat: rec.VLat * 180. / math.Pi,
					}),
				)
			}
		}
		if *incDepth {
			vehicle.Add(
				kml.ExtendedData(
					kml.SchemaData("#data_schema", depth),
				),
			)
		}
		shipTrack.Add(ship)
		vehicleTrack.Add(vehicle)
		fin.Close()
		bar.Incr()
	}

	k := kml.GxKML(
		kml.Document(
			kml.Name(*docName),
			kml.SharedStyle(
				"shipStyle",
				kml.IconStyle(
					kml.Icon(kml.Href(icon.TrackHref(0))),
					kml.Color(color.RGBA{R: 0xf0, B: 0xf0, A: 0xff}),
				),
				kml.LineStyle(
					kml.Width(1.0),
					kml.Color(color.RGBA{R: 0xf0, B: 0xf0, A: 0xff}),
				),
			),
			kml.SharedStyle(
				"vehicleStyle",
				kml.IconStyle(
					kml.Icon(kml.Href(icon.TrackHref(0))),
					kml.Color(color.RGBA{G: 0xf0, A: 0xff}),
				),
				kml.LineStyle(
					kml.Width(0.8),
					kml.Color(color.RGBA{G: 0xf0, A: 0xff}),
				),
			),
			schema,
			kml.Placemark(
				kml.Name("R/V Robertson"),
				kml.Description("Vessel track"),
				kml.StyleURL("#shipStyle"),
				shipTrack,
			),
			kml.Placemark(
				kml.Name("FOCUS"),
				kml.Description("Towbody track"),
				kml.StyleURL("#vehicleStyle"),
				vehicleTrack,
			),
			regions,
		),
	)
	bar.Incr()

	if err := k.WriteIndent(fout, "", "  "); err != nil {
		log.Fatal(err)
	}
}
