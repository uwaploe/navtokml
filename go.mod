module bitbucket.org/uwaploe/navtokml

require (
	bitbucket.org/uwaploe/go-must v0.8.0
	bitbucket.org/uwaploe/navsvc v1.7.0
	github.com/BurntSushi/toml v0.3.1
	github.com/gosuri/uilive v0.0.3 // indirect
	github.com/gosuri/uiprogress v0.0.1
	github.com/twpayne/go-kml v1.5.2
)

go 1.13
